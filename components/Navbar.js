import {Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';

export const Navbar = ({navigation}) => {
  const handleList = () => {
    navigation.navigate('ListUser');
  };

  const handleNewUser = () => {
    navigation.navigate('NewUser');
  };

  return (
    <View>
      <Pressable
        style = {styles.backgroundNavButton}
        onPress={handleList}>
        <Text style={styles.textNavButton}>Liste d'utilisateur</Text>
      </Pressable>

      <Pressable
        style = {styles.backgroundNavButton}
        onPress={handleNewUser}>
        <Text style={styles.textNavButton}>Ajouter nouvel utilisateur</Text>
      </Pressable>
    </View>
  );
};

export default Navbar;

const styles = StyleSheet.create({
  backgroundNavButton : {
    backgroundColor : "#577590",
    alignItems : 'center',
    alignSelf : 'center',
    width : 300,
    borderColor : 'black',
    margin : 5,
    borderWidth : 1,
  },
  textNavButton : {
    color : 'white',
    fontSize : 20,
    padding : 15,
    
    
  }
});
