import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import Navbar from '../components/Navbar';

const UtilisateurEntrant = (nouvelUtlisateur) => {
    
}

export const Home = ({route, navigation}) => {
    const {nomKey , prenomKey} = route.params || ""
    return (
    <View style={{flex:1}}>
      <Navbar navigation={navigation}></Navbar>
      <Text style={styles.TextContent}>Utilisateur : {nomKey} {prenomKey}</Text>
        
    {/* Ici il y a très probablement un map() pour boucler sur les utilateurs de mon composant NewUser */}


    </View>
  );
}

export default Home

const styles = StyleSheet.create({
  TextContent : {
    color : 'black',
    fontSize : 20,
    textAlign :'center',
  }
});
