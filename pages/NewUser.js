import {StyleSheet, Text, View, TextInput, Pressable} from 'react-native';
import React, {useState} from 'react';
import Navbar from '../components/Navbar';

export const NewUser = ({navigation}) => {
  const [nom, setNom] = useState("")
  const [prenom, setPrenom] = useState("");

  // const [nouvelUtilisateur, setNouvelUtilisateur] = useState({nom, prenom})


  const handleNewUser = () => {

    // setNouvelUtilisateur([...nouvelUtilisateur, {}])
    // console.log(nouvelUtilisateur);

    navigation.navigate('ListUser', {nomKey : nom, prenomKey : prenom})
  } 

  return (
    <View style={{flex : 1}}>
      <Navbar navigation={navigation}></Navbar>
      <TextInput
        style={styles.input}
        onChangeText={value => setNom(value)}
        placeholder="Nom"
      />
      <TextInput
        style={styles.input}
        onChangeText={value => setPrenom(value)}
        placeholder="Prénom"
      />
      <Pressable style={styles.sentInfo} onPress={handleNewUser}>
        <Text style={styles.pressText}>Valider</Text>
      </Pressable>
    </View>
  );
};

export default NewUser;

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },

  sentInfo : {
    alignItems : "center",
    backgroundColor : "#f9c74f",
    padding : 15,
    borderWidth: 1,
  },

  pressText : {
    color : "white",
    fontSize : 20,
    
  }
});