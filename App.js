import { StyleSheet} from 'react-native';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NewUser from './pages/NewUser';
import ListUser from './pages/ListUser';

const Stack = createNativeStackNavigator();

export const App = () => {

  return (

    <NavigationContainer>
        <Stack.Navigator initialRouteName='ListUser'>
        <Stack.Screen name='ListUser' component={ListUser} options={{ title : 'Liste d\'utilisateur'}} />
          <Stack.Screen name='NewUser' component={NewUser} options={{ title : 'Nouvel utilisateur'}} />

        </Stack.Navigator>

    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
